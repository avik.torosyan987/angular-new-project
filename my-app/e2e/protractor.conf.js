const { SpecReporter } = require('jasmine-spec-reporter');
exports.config = {
  allScriptsTimeout: 11000,
  specs: [
    // spec paths
    'e2e/app.e2e-spec.ts'
  ],
  capabilities: {
    browserName: 'chrome',
    chromeOptions: {
      args: [ '--headless', '--no-sandbox', '--disable-gpu', '--window-size=1920,1080' ]
    }
  },
  maxSessions: 2,
  directConnect: true,
  baseUrl: 'http://localhost:4200/',
  framework: 'jasmine',
  jasmineNodeOpts: {
    showColors: true,
    defaultTimeoutInterval: 300000,
    print: function() {}
  },
  onPrepare() {
    require('ts-node').register({
      project: 'e2e/tsconfig.e2e.json'
    });
    jasmine.getEnv().addReporter(new SpecReporter({ spec: { displayStacktrace: true } }));
  }
};
